/***** Instructions for basic use *******/

Download the latest commit, then enable module.

Once enabled, you need to grab your API key from your teamwork profile, then fill out the settings form at yourdrupalsite.com/admin/config/services/teamwork

Now you can Add a task at yourdrupalsite.comadmin/config/services/teamwork/addtask

/**** Rules Integration ****/

There are two rules actions available. You can create a task on a rules event, or you can update your status on a rules event.